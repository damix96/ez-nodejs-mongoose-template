var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var path =require('path');
// всякие нужные модули. можно забить.
// и вообще на этот файл можно забить.

const app = express()
const Port = 3000;

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
// Говорим использовать модуль bodyParser для обработки запросов.

mongoose.connect('mongodb://localhost:27017/sosiska');
var dbc = mongoose.connection;
dbc.once('open', function() {
  console.log('Подключение к базе данных прошло успешно ^^');
});
// Подключение к базе данных. Забей просто. Потом разберешься.

app.use(express.static('static/'))
// Тут мы говорим, что если кто-то "постучится" на наш сервер,
// По адресу: {base_url}/static/....
// и наткнется на какой- нибудь файл который лежит в папке '/static',
// то мы позволим ему скачать его. Я обычно тут храню html страницы.


var api = require('./routes/index')

app.use('/api', api)
// Тут мы говорим что для оброботки запросов по пути: {Base_url}/api/...
// будет использоваться файл ./routes/index



app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send({ message: err.message })
})
/// тут хз что мы говорим. Походу просто обрабатываем ошибка сервера.


app.listen(Port, () => {
  console.log('Сервер запущен на порту '+Port);
});
// А тут мы запускаем наш сервер на порту `Port`


module.exports = app
// Позволяем импортировать этот файл.
