var express = require('express');
var router = express.Router();
var User = require('../models/User')

router.get('/get', function (req, res) {
  // когда кто-нибудь делает GET запрос по адресу /api/get
  // вызывается Эта анонимная функция ( function (req, res){...} )
  console.log( 'Я получил GET запрос с параметрами: ', req.query ) // в GET запросе нету body поэтому мы можем получить данные только через query
  // выводит в консоль данные которые нам пришли.
  res.send('Привет, уродец))0)')
  // отправляется ответ. req - содержит Запрос, а res - отправляет ответ.
})

router.post('/post', function (req, res) {
  // А это уже обработка POST запроса.
  console.log( 'Я получил POST запрос с параметрами: ', req.body ) // в POST запросе есть и query и body.

  res.send('Красаааава!')

})

router.post('/user', function (req, res) {
  // теперь работа с Базой данных.
  // Выше мы объявили переменную, в которой хранится модель базы данных
  // Обращаясь к этой модели, мы можем находить или создавать новые записи
  new User({
    name: req.body.name,
    group: req.body.group,
    sex: req.body.sex
  }).save(function(err, savedUser) {
    if(err) {
      return res.send({ err }) // возвращаем ответ с ошибкой
    } // если при сохранении произошла ошибка, то возвращем ответ с ошибкой

    res.send({msg: 'Я сохранил человека!', user: savedUser})
  })
})

router.get('/user', function (req, res) {
  // Выше мы создали апишку для сохранения юзера.
  // Теперь попытаемся Получить список сохраненных юзеров!

  User.find({}) //тут говорим найти все записи.
                // если нужно найти определенного человека с именем петя,
                // нужно написать User.find({name: 'Петя'})

  .exec(function (err, foundUsersArray) {
    res.send(foundUsersArray) // сразу отправим без проверок на ошибки.
  })
})


module.exports = router;
// позволяем использовать этот файл (грубо говоря)

// когда где-то делается импорт этого файла (как в server.js) ему возвращается данный router
